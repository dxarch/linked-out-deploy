describe('API Tests', () => {
    it('should return a valid response', async () => {
      const response = await axios.get('/api/data');
      expect(response.status).toBe(200);
      expect(response.data).toHaveProperty('result');
      expect(response.data.result).toHaveLength(10);
    });
  
    it('should create a new resource', async () => {
      const payload = { name: 'Test Resource' };
      const response = await axios.post('/api/resources', payload);
      expect(response.status).toBe(201);
      expect(response.data).toHaveProperty('id');
    });
  
    it('should update an existing resource', async () => {
      const resourceId = '123456';
      const payload = { name: 'Updated Resource' };
      const response = await axios.put(`/api/resources/${resourceId}`, payload);
      expect(response.status).toBe(200);
      expect(response.data).toHaveProperty('message', 'Resource updated successfully');
    });
  
    // Другие тесты для API...
  });